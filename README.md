# Outsourced Cinenso HTML templating tasks

## Content
This package should contain all the necessary resources to start building the tasks detailed below:
- ./static - hosts all the images on the templates
- ./cinenso_theme.css - is the main bootstrap theme file
- ./cinenso_mobile.css - is an overlay for mobile on top of the theme file
- ./cinenso_desktop.css - is an overlay for the desktop verison 
- ./style_tester.html - is a boostrap control guide with the cinenso_theme overlay so you get the idea of the theme in general
- ./base_desktop.html - is the base template for the desktop version of the tasks
- ./base_mobile.html - is the base template for the mobile versions of the tasks
- ./alpha-assets-...zip - extra images that are needed for this task 

## Figma
Our UI design prototype is on figma.com I'll put a direct link to each item to the tasks below but here is a general Style guide:
https://www.figma.com/file/SruO1eFiW0VAUtp49uZ2CZ/user-flow?node-id=1454%3A454

Much of the required css-classes for the style guide is already in the cinenso_theme.css.

## Tasks

### General remarks
- Please avoid using inline css
- If you cannot find css classes in the theme that fits your need. If you need to use new css classes please create 
them in cinenso_theme.css and use 
those
- Please use the general principles of bootstrap for styling (margins, padding, flexboxes, grids, control groups ...)
We use bootstrap 4.6.0
https://getbootstrap.com/docs/4.0/getting-started/introduction/
- Icons used are material design icons available here:
https://material.io/resources/icons/?style=baseline
- In case you are missing an image or other material please reach out to: marton.sebok@cinenso.com

### 1. Create the Channel page (Desktop)
Figma:
https://www.figma.com/file/SruO1eFiW0VAUtp49uZ2CZ/user-flow?node-id=2556%3A451

- Create the "Channel page (Logged-in)" page detailed on figma above. (The page is the 2nd from the right)
- Use base_desktop.html as a foundation

### 2. Create the Channel page (Mobile)
Figma:
https://www.figma.com/file/SruO1eFiW0VAUtp49uZ2CZ/user-flow?node-id=2556%3A451

- Create the "Mobile channel 1" page detailed on figma above. (The page is the 1st from the right)
- Use base_mobile.html as a foundation

### 3. Create the Sales Page (Desktop)
Figma:
https://www.figma.com/file/SruO1eFiW0VAUtp49uZ2CZ/user-flow?node-id=2562%3A0

- Create the "Sales page" page detailed on figma above. (The page is the 4th and 5th from the left)
- Use base_desktop.html as a foundation
- FAQ section is not required


### 4. Create the Sales page (Mobile)
Figma:
https://www.figma.com/file/SruO1eFiW0VAUtp49uZ2CZ/user-flow?node-id=2562%3A0

- Create the "Sales page - mobile" page detailed on figma above. (The page is the 1st 3 from the left)
- Use base_mobile.html as a foundation
- FAQ section is not required
- PLease use bootstrap accordions for the collapsable items:
https://getbootstrap.com/docs/4.0/components/collapse/#accordion-example